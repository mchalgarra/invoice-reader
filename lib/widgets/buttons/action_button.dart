import 'package:flutter/material.dart';

class ActionButton extends StatelessWidget {
  const ActionButton(
      {Key? key,
      this.tooltip,
      this.backgroundColor,
      this.foregroundColor,
      required this.onPressed,
      required this.icon})
      : super(key: key);

  final VoidCallback onPressed;
  final Widget icon;

  final String? tooltip;
  final Color? backgroundColor;
  final Color? foregroundColor;

  @override
  Widget build(BuildContext context) {
    return Material(
        shape: const CircleBorder(),
        clipBehavior: Clip.antiAlias,
        color: backgroundColor ?? Colors.white,
        elevation: 4.0,
        child: IconButton(
            tooltip: tooltip,
            onPressed: () {
              onPressed();
            },
            icon: icon));
  }
}
