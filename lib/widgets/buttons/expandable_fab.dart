import 'package:flutter/material.dart';
import 'package:invoice_reader/widgets/buttons/action_button.dart';
import 'package:invoice_reader/widgets/buttons/expanding_action_button.dart';

class ExpandableFAB extends StatefulWidget {
  const ExpandableFAB({
    Key? key,
    this.disabled,
    this.onPressed,
    this.iconSlot,
    this.tooltip,
    required this.backgroundColor,
    required this.foregroundColor,
    required this.distance,
    required this.children,
  }) : super(key: key);

  final VoidCallback? onPressed;
  final String? tooltip;

  final bool? disabled;

  final double distance;
  final List<Widget> children;
  final Widget? iconSlot;

  final Color backgroundColor;
  final Color foregroundColor;

  @override
  State<ExpandableFAB> createState() => _ExpandableFABState();
}

class _ExpandableFABState extends State<ExpandableFAB>
    with SingleTickerProviderStateMixin {
  late final AnimationController _controller;
  late final Animation<double> _expandAnimation;
  bool _open = false;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      value: _open ? 1.0 : 0.0,
      duration: const Duration(milliseconds: 250),
      vsync: this,
    );

    _expandAnimation = CurvedAnimation(
      curve: Curves.fastOutSlowIn,
      reverseCurve: Curves.easeOutQuad,
      parent: _controller,
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _toggle() {
    setState(() {
      _open = !_open;
      _open ? _controller.forward() : _controller.reverse();
    });
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox.expand(
      child: Stack(
        alignment: Alignment.bottomRight,
        clipBehavior: Clip.none,
        children: [
          ..._buildExpandingActionButtons(),
          _buildFAB(),
        ],
      ),
    );
  }

  List<Widget> _buildExpandingActionButtons() {
    final children = <Widget>[];
    final count = widget.children.length;
    final step = 90.0 / (count - 1);

    for (var i = 0, angleInDegrees = 0.0;
        i < count;
        i++, angleInDegrees += step) {
      ActionButton actionButton = widget.children[i] as ActionButton;

      children.add(
        ExpandingActionButton(
          directionInDegrees: angleInDegrees,
          maxDistance: widget.distance,
          progress: _expandAnimation,
          child: ActionButton(
            onPressed: () {
              _toggle();
              actionButton.onPressed();
            },
            icon: actionButton.icon,
            tooltip: actionButton.tooltip,
            backgroundColor: actionButton.backgroundColor,
            foregroundColor: actionButton.foregroundColor,
          ),
        ),
      );
    }

    return children;
  }

  Widget _buildFAB() {
    return AnimatedContainer(
      transformAlignment: Alignment.center,
      transform:
          Matrix4.diagonal3Values(_open ? 0.8 : 1.0, _open ? 0.8 : 1.0, 1.0),
      duration: const Duration(milliseconds: 250),
      curve: const Interval(0.0, 0.5, curve: Curves.easeOut),
      child: FloatingActionButton(
          onPressed: () {
            if (widget.onPressed != null) {
              widget.onPressed!();
            }

            if (widget.disabled ?? false) {
              return;
            }

            _toggle();
          },
          tooltip: widget.tooltip,
          backgroundColor: widget.backgroundColor,
          child: AnimatedRotation(
              turns: _open ? 0.375 : 0,
              duration: const Duration(milliseconds: 250),
              child: widget.iconSlot ??
                  Icon(Icons.add, color: widget.foregroundColor))),
    );
  }
}
