import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:invoice_reader/services/theme_service.dart';

class InvoiceOption {
  String label;
  Color color;
  Function onPressed;

  InvoiceOption(
      {required this.label, required this.color, required this.onPressed});
}

class InvoiceOptionsModal extends StatelessWidget {
  const InvoiceOptionsModal(
      {Key? key, required this.options, required this.title})
      : super(key: key);

  final List<InvoiceOption> options;
  final String title;

  ThemeService get themeService => GetIt.I<ThemeService>();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: const EdgeInsets.all(24),
          child: Text(title,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                  color: themeService.textColor)),
        ),
        ...options.asMap().entries.map((entry) => Padding(
              padding: EdgeInsets.only(
                  left: 24,
                  right: 24,
                  top: 8,
                  bottom: entry.key == options.length - 1 ? 24 : 8),
              child: SizedBox(
                  width: double.infinity,
                  height: 50,
                  child: OutlinedButton(
                      onPressed: () => entry.value.onPressed(),
                      style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(
                              entry.value.color.withOpacity(0.05)),
                          side: MaterialStateProperty.all(
                              BorderSide(color: entry.value.color)),
                          shape: MaterialStateProperty.all(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50)))),
                      child: Text(entry.value.label,
                          style: TextStyle(
                              color: entry.value.color,
                              fontSize: 16,
                              fontWeight: FontWeight.w600)))),
            ))
      ],
    );
  }
}
