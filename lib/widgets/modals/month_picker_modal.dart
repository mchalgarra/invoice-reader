import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:invoice_reader/services/theme_service.dart';
import 'package:invoice_reader/utils/date.dart';

class MonthPickerModal extends StatefulWidget {
  const MonthPickerModal(
      {Key? key, required this.startYear, required this.onSelect})
      : super(key: key);

  final int startYear;
  final Function onSelect;

  @override
  State<MonthPickerModal> createState() => _MonthPickerModalState();
}

class _MonthPickerModalState extends State<MonthPickerModal> {
  ThemeService get themeService => GetIt.I<ThemeService>();

  int year = DateTime.now().year;

  @override
  void initState() {
    year = widget.startYear;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    /*24 is for notification bar on Android*/
    const double itemHeight = 50;
    final double itemWidth = size.width / 3;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          margin:
              const EdgeInsets.only(left: 24, right: 24, top: 20, bottom: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Select month and year',
                  style: TextStyle(
                      color: themeService.textColor,
                      fontSize: 18,
                      fontWeight: FontWeight.w500)),
            ],
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            IconButton(
                onPressed: () {
                  setState(() {
                    year--;
                  });
                },
                splashRadius: 20,
                icon: Icon(Icons.remove, color: themeService.textColor)),
            Text(year.toString(),
                style: TextStyle(
                    color: themeService.accentTextColor,
                    fontSize: 20,
                    fontWeight: FontWeight.w600)),
            IconButton(
                onPressed: () {
                  setState(() {
                    year++;
                  });
                },
                splashRadius: 20,
                icon: Icon(Icons.add, color: themeService.textColor)),
          ],
        ),
        SizedBox(
            height: 280,
            width: double.infinity,
            child: GridView.count(
              childAspectRatio: itemWidth / itemHeight,
              padding: const EdgeInsets.all(20),
              crossAxisCount: 3,
              mainAxisSpacing: 20,
              children: [
                ...DateUtil.months.asMap().entries.map((entry) => Center(
                    child: GestureDetector(
                        child: Text(
                          entry.value,
                          style: TextStyle(
                              color: themeService.textColor,
                              fontSize: 16,
                              fontWeight: FontWeight.w600),
                        ),
                        onTap: () => widget.onSelect(entry.key + 1, year))
                    // color: Colors.amber,
                    ))
              ],
            ))
      ],
    );
  }
}
