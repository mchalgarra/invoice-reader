import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:invoice_reader/services/theme_service.dart';

class InvoiceTitleModal extends StatefulWidget {
  const InvoiceTitleModal({
    Key? key,
    this.initialTitle,
    this.onCancel,
    required this.onSave,
  }) : super(key: key);

  final Function onSave;
  final VoidCallback? onCancel;
  final String? initialTitle;

  @override
  State<InvoiceTitleModal> createState() => _InvoiceTitleModalState();
}

class _InvoiceTitleModalState extends State<InvoiceTitleModal>
    with SingleTickerProviderStateMixin {
  ThemeService themeService = GetIt.I<ThemeService>();

  late AnimationController animationController;
  late Animation<double> buttonSizeAnimation;

  late TextEditingController _controller;

  bool loading = false;

  @override
  void initState() {
    _controller = TextEditingController(text: widget.initialTitle ?? '');

    super.initState();

    animationController = AnimationController(
        duration: const Duration(milliseconds: 300), vsync: this);

    buttonSizeAnimation = Tween<double>(begin: 120, end: 50).animate(
        CurvedAnimation(
            parent: animationController, curve: Curves.fastOutSlowIn))
      ..addListener(() {
        setState(() {});
      });
  }

  @override
  Widget build(BuildContext context) {
    final keyboardPadding = MediaQuery.of(context).viewInsets.bottom;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 32.0, vertical: 32.0),
          child: TextField(
              readOnly: loading,
              controller: _controller,
              style: TextStyle(color: themeService.textColor),
              cursorColor: themeService.accentColor,
              decoration: InputDecoration(
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: themeService.accentColor),
                    borderRadius: BorderRadius.circular(50.0)),
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: themeService.darkTheme
                            ? Colors.white
                            : Colors.black87),
                    borderRadius: BorderRadius.circular(50.0)),
                contentPadding: const EdgeInsets.all(20.0),
                floatingLabelStyle: TextStyle(color: themeService.accentColor),
                labelText: 'Invoice title',
                labelStyle: TextStyle(
                    fontFamily: 'Poppins',
                    fontSize: 16.0,
                    backgroundColor: Colors.transparent,
                    color: themeService.textColor),
              )),
        ),
        Padding(
            padding: EdgeInsets.only(
                bottom: keyboardPadding + 24, left: 32, right: 32),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(
                  height: 50,
                  width: 120,
                  child: TextButton(
                      onPressed: () {
                        if (loading) {
                          return;
                        }

                        if (widget.onCancel != null) {
                          widget.onCancel!();
                        }

                        Navigator.pop(context);
                      },
                      style: ButtonStyle(
                          padding: MaterialStateProperty.all(
                              const EdgeInsets.all(0)),
                          shape: MaterialStateProperty.all(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50.0))),
                          backgroundColor: MaterialStateProperty.all(!loading
                              ? themeService.alertColor
                              : Colors.grey)),
                      child: const Text('CANCEL',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                              letterSpacing: 1.2))),
                ),
                SizedBox(
                    height: 50,
                    width: buttonSizeAnimation.value,
                    child: TextButton(
                        onPressed: () async {
                          if (loading) {
                            return;
                          }

                          FocusScope.of(context).unfocus();
                          setState(() {
                            animationController.forward();
                            loading = true;
                          });

                          try {
                            await widget.onSave(_controller.value.text);
                          } finally {
                            setState(() {
                              animationController.reverse();
                              loading = false;
                            });
                          }
                        },
                        style: ButtonStyle(
                            padding: MaterialStateProperty.all(
                                const EdgeInsets.all(0)),
                            shape: MaterialStateProperty.all(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(50.0))),
                            backgroundColor: MaterialStateProperty.all(
                                themeService.accentColor)),
                        child: loading
                            ? const SizedBox(
                                width: 24,
                                height: 24,
                                child: CircularProgressIndicator(
                                  color: Colors.white,
                                  strokeWidth: 3,
                                ))
                            : const Text('SAVE',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16,
                                    letterSpacing: 1.2))))
              ],
            ))
      ],
    );
  }
}
