import 'package:flutter/material.dart';

class ErrorDialog extends StatelessWidget {
  const ErrorDialog({Key? key, required this.message}) : super(key: key);

  final String message;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text(
        'Oops!',
        style: TextStyle(color: Colors.red),
      ),
      content: Text(message),
      actions: [
        TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Text('OK',
                style: TextStyle(
                    color: Colors.red, fontFamily: 'Poppins', fontSize: 16)))
      ],
    );
  }
}
