part of 'invoice_bloc.dart';

abstract class InvoiceEvent {
  const InvoiceEvent();

  @override
  // ignore: override_on_non_overriding_member
  List<Object> get props => [];
}

class FetchInvoices extends InvoiceEvent {
  final int month;
  final int year;
  const FetchInvoices(this.month, this.year);

  @override
  List<Object> get props => [month, year];
}

class AddInvoice extends InvoiceEvent {
  final InvoiceModelCreate invoice;
  final bool keepImage;
  const AddInvoice(this.invoice, this.keepImage);

  @override
  List<Object> get props => [invoice, keepImage];
}

class UpdateInvoice extends InvoiceEvent {
  final InvoiceModel invoice;
  const UpdateInvoice(this.invoice);

  @override
  List<Object> get props => [invoice];
}

class RemoveInvoice extends InvoiceEvent {
  final String id;
  final String imagePath;
  const RemoveInvoice(this.id, this.imagePath);

  @override
  List<Object> get props => [id, imagePath];
}
