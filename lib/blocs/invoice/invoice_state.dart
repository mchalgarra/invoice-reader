part of 'invoice_bloc.dart';

abstract class InvoiceState {
  List<InvoiceModel> invoices = [];

  InvoiceState(List<InvoiceModel> _invoices);

  @override
  // ignore: override_on_non_overriding_member
  List<Object> get props => [invoices];
}

class InvoiceInitial extends InvoiceState {
  InvoiceInitial() : super([]);
}

class InvoiceChanged extends InvoiceState {
  // ignore: annotate_overrides, overridden_fields
  final List<InvoiceModel> invoices;
  InvoiceChanged({required this.invoices}) : super(invoices);
}

class InvoiceError extends InvoiceState {
  final String message;
  InvoiceError({required this.message}) : super([]);
}
