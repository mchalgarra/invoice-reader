import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:invoice_reader/models/invoice_model.dart';
import 'package:invoice_reader/repositories/invoice_repository_impl.dart';

part 'invoice_event.dart';
part 'invoice_state.dart';

class InvoiceBloc extends Bloc<InvoiceEvent, InvoiceState> {
  final InvoiceRepository _invoiceRepository;

  InvoiceBloc(this._invoiceRepository) : super(InvoiceInitial()) {
    on<FetchInvoices>((event, emit) async =>
        await _fetchInvoices(emit, event.month, event.year));
    on<AddInvoice>((event, emit) async =>
        await _addInvoice(emit, event.invoice, event.keepImage));
    on<UpdateInvoice>((event, emit) => _updateInvoice(emit, event.invoice));
    on<RemoveInvoice>(
        (event, emit) => _removeInvoice(emit, event.id, event.imagePath));
  }

  Future<void> _fetchInvoices(
      Emitter<InvoiceState> emit, int month, int year) async {
    try {
      List<InvoiceModel> invoices =
          await _invoiceRepository.fetchInvoices(month, year);

      emit(InvoiceChanged(invoices: invoices));
    } on Exception {
      emit(InvoiceError(message: 'Could not fetch the invoices!'));
    }
  }

  Future<void> _addInvoice(Emitter<InvoiceState> emit,
      InvoiceModelCreate invoice, bool keepImage) async {
    try {
      List<InvoiceModel> invoices =
          await _invoiceRepository.addInvoice(invoice, keepImage);

      emit(InvoiceChanged(invoices: invoices));
    } on Exception {
      emit(InvoiceError(message: 'Could not add the invoice!'));
    }
  }

  void _updateInvoice(Emitter<InvoiceState> emit, InvoiceModel invoice) {
    try {
      List<InvoiceModel> invoices = _invoiceRepository.updateInvoice(invoice);
      emit(InvoiceChanged(invoices: invoices));
    } on Exception {
      emit(InvoiceError(message: 'Could not add the invoice!'));
    }
  }

  void _removeInvoice(Emitter<InvoiceState> emit, String id, String imagePath) {
    try {
      List<InvoiceModel> invoices =
          _invoiceRepository.removeInvoice(id, imagePath);

      emit(InvoiceChanged(invoices: invoices));
    } on Exception {
      emit(InvoiceError(message: 'Could not add the invoice!'));
    }
  }
}
