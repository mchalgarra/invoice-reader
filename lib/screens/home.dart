import 'dart:async';
import 'dart:io';
import 'dart:developer' as developer;

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:image_picker/image_picker.dart';
import 'package:invoice_reader/blocs/invoice/invoice_bloc.dart';
import 'package:invoice_reader/models/invoice_model.dart';
import 'package:invoice_reader/screens/camera.dart';
import 'package:invoice_reader/screens/image_area_selector.dart';
import 'package:invoice_reader/services/camera_service.dart';
import 'package:invoice_reader/services/theme_service.dart';
import 'package:invoice_reader/utils/date.dart';
import 'package:invoice_reader/views/invoices_table.dart';
import 'package:invoice_reader/widgets/buttons/action_button.dart';
import 'package:invoice_reader/widgets/buttons/expandable_fab.dart';
import 'package:invoice_reader/widgets/shared/error_dialog.dart';
import 'package:invoice_reader/widgets/modals/invoice_options_modal.dart';
import 'package:invoice_reader/widgets/modals/month_picker_modal.dart';

class HomeView extends StatefulWidget {
  const HomeView({Key? key, this.onBuild}) : super(key: key);

  final String title = 'Hey there';
  final Function? onBuild;

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  ThemeService get themeService => GetIt.I<ThemeService>();
  CameraService get cameraService => GetIt.I<CameraService>();

  final Connectivity _connectivity = Connectivity();
  late StreamSubscription<ConnectivityResult> _connectivitySubscription;
  bool online = false;

  bool isCamerasLoading = false;
  bool darkTheme = true;
  bool loading = false;

  int currentMonth = DateTime.now().month;
  int currentYear = DateTime.now().year;

  @override
  void initState() {
    isCamerasLoading = cameraService.cameras.isEmpty;
    darkTheme = themeService.darkTheme;

    if (widget.onBuild != null) {
      widget.onBuild!();
    }

    super.initState();

    initConnectivity();

    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);

    if (cameraService.cameras.isEmpty) {
      cameraService
          .loadCameras()
          .whenComplete(() => setState(() => isCamerasLoading = false));
    }
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  Future<void> initConnectivity() async {
    late ConnectivityResult result;
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      developer.log('Couldn\'t check connectivity status', error: e);
      return;
    }

    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    setState(() {
      online = result == ConnectivityResult.mobile ||
          result == ConnectivityResult.wifi;
    });
  }

  Widget _getMonthText() {
    return Row(
      children: [
        Padding(
            padding: const EdgeInsets.only(left: 16, right: 8),
            child: Icon(Icons.calendar_month_rounded,
                color: themeService.accentTextColor)),
        Text('${DateUtil.months[currentMonth - 1]}, $currentYear',
            style: TextStyle(
                color: themeService.accentTextColor,
                fontSize: 20,
                fontWeight: FontWeight.w600))
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        child: Scaffold(
          appBar: AppBar(
            title: Text(
              widget.title,
              style: TextStyle(
                  fontFamily: 'Poppins',
                  fontSize: 24,
                  fontWeight: FontWeight.w500,
                  color: themeService.textColor),
            ),
            backgroundColor: Colors.transparent,
            toolbarHeight: 80,
            shadowColor: Colors.transparent,
            automaticallyImplyLeading: false,
            actions: <Widget>[
              Container(
                margin: const EdgeInsets.only(right: 16.0),
                child: (IconButton(
                  splashRadius: 24.0,
                  padding: const EdgeInsets.all(0.0),
                  color:
                      Color(themeService.darkTheme ? 0xFFFFDD87 : 0xFF87D4FF),
                  icon: Icon(themeService.darkTheme
                      ? Icons.light_mode
                      : Icons.dark_mode),
                  onPressed: () {
                    themeService.toggleTheme();
                    setState(() {
                      darkTheme = !darkTheme;
                    });
                  },
                )),
              ),
            ],
          ),
          body: Container(
            margin: const EdgeInsets.only(top: 8.0),
            child: BlocBuilder<InvoiceBloc, InvoiceState>(
                builder: (context, state) {
              if (state is InvoiceInitial) {
                context.read<InvoiceBloc>().add(
                    FetchInvoices(DateTime.now().month, DateTime.now().year));
              } else if (state is InvoiceChanged && !loading) {
                return Stack(
                  children: [
                    Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                      GestureDetector(
                        child: _getMonthText(),
                        onTap: () {
                          showModalBottomSheet(
                              context: context,
                              backgroundColor: themeService.backgroundColor,
                              isDismissible: true,
                              isScrollControlled: true,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20.0)),
                              builder: (context) => MonthPickerModal(
                                    startYear: currentYear,
                                    onSelect: (month, year) async {
                                      if (currentMonth == month &&
                                          currentYear == year) {
                                        Navigator.pop(context);
                                        return;
                                      }

                                      setState(() {
                                        loading = true;
                                        currentMonth = month;
                                        currentYear = year;
                                      });

                                      context
                                          .read<InvoiceBloc>()
                                          .add(FetchInvoices(month, year));

                                      Navigator.pop(context);

                                      await Future.delayed(
                                          const Duration(seconds: 2));

                                      setState(() {
                                        loading = false;
                                      });
                                    },
                                  ));
                        },
                      )
                    ]),
                    Container(
                      margin: const EdgeInsets.only(top: 48),
                      child: InvoicesTable(
                        invoices: [...state.invoices]
                            .where((i) =>
                                i.month == currentMonth &&
                                i.year == currentYear)
                            .toList(),
                        onRowTap: (InvoiceModel invoice) {
                          showModalBottomSheet(
                              context: context,
                              backgroundColor: themeService.backgroundColor,
                              isDismissible: true,
                              isScrollControlled: true,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20.0)),
                              builder: (context) => InvoiceOptionsModal(
                                      title:
                                          '${invoice.number} | ${invoice.name}',
                                      options: [
                                        InvoiceOption(
                                            label: 'Editar',
                                            color: Colors.orange,
                                            onPressed: () {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          ImageAreaSelector(
                                                              image: File(
                                                                  invoice
                                                                      .image),
                                                              invoice:
                                                                  invoice)));
                                            }),
                                        InvoiceOption(
                                            label: 'Excluir',
                                            color: Colors.red,
                                            onPressed: () {
                                              context.read<InvoiceBloc>().add(
                                                  RemoveInvoice(invoice.id,
                                                      invoice.image));

                                              Navigator.pop(context);
                                            }),
                                      ]));
                        },
                      ),
                    )
                  ],
                );
              }

              return SizedBox(
                width: double.infinity,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      _getMonthText(),
                      Container(
                          margin: const EdgeInsets.only(top: 40),
                          child: CircularProgressIndicator(
                            color: themeService.accentTextColor,
                          ))
                    ]),
              );
            }),
          ),
          backgroundColor: themeService.backgroundColor,
          floatingActionButton: ExpandableFAB(
              onPressed: () {
                if (!online) {
                  showDialog(
                      context: context,
                      builder: (context) => const ErrorDialog(
                          message:
                              'No internet connection... Please, try again later.'));
                }
              },
              distance: 80,
              tooltip: 'Add invoice',
              backgroundColor:
                  online ? themeService.accentColor : themeService.alertColor,
              foregroundColor: Colors.white,
              disabled: isCamerasLoading || !online,
              iconSlot: isCamerasLoading
                  ? const SizedBox(
                      width: 24,
                      height: 24,
                      child: CircularProgressIndicator(
                        color: Colors.white,
                        strokeWidth: 3,
                      ))
                  : Icon(
                      online ? Icons.add : Icons.wifi_tethering_error_rounded,
                      size: 30),
              children: [
                ActionButton(
                    icon: const Icon(Icons.camera_alt, color: Colors.white),
                    backgroundColor: themeService.accentColor,
                    tooltip: 'Take photo',
                    onPressed: () async {
                      await Future.delayed(const Duration(milliseconds: 300));

                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const CameraView()));
                    }),
                ActionButton(
                    icon: const Icon(Icons.insert_photo_rounded,
                        color: Colors.white),
                    backgroundColor: themeService.accentColor,
                    tooltip: 'From gallery',
                    onPressed: () async {
                      await Future.delayed(const Duration(milliseconds: 300));

                      XFile? image = await ImagePicker()
                          .pickImage(source: ImageSource.gallery);

                      if (image == null) {
                        return;
                      }

                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  ImageAreaSelector(image: File(image.path))));
                    })
              ]),
        ),
        onWillPop: () async => false);
  }
}
