import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:invoice_reader/services/camera_service.dart';
import 'package:invoice_reader/services/theme_service.dart';
import 'package:invoice_reader/screens/image_area_selector.dart';
import 'package:invoice_reader/views/camera_container.dart';
import 'package:invoice_reader/widgets/shared/error_dialog.dart';

class CameraView extends StatefulWidget {
  const CameraView({Key? key}) : super(key: key);

  @override
  State<CameraView> createState() => _CameraViewState();
}

class _CameraViewState extends State<CameraView> {
  ThemeService get themeService => GetIt.I<ThemeService>();
  CameraService get cameraService => GetIt.I<CameraService>();

  late CameraController _controller;
  late Future<void> _initializeControllerFuture;

  @override
  void initState() {
    super.initState();

    _controller = CameraController(
        cameraService.cameras.first, ResolutionPreset.max,
        enableAudio: false, imageFormatGroup: ImageFormatGroup.jpeg);

    _initializeControllerFuture = _controller.initialize();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: themeService.backgroundColor,
      body: FutureBuilder<void>(
        future: _initializeControllerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return CameraContainer(cameraPreview: CameraPreview(_controller));
          } else {
            return Center(
                child:
                    CircularProgressIndicator(color: themeService.accentColor));
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: themeService.accentColor,
        child: const Icon(Icons.camera_alt),
        onPressed: () async {
          try {
            await _initializeControllerFuture;

            await _controller.setFlashMode(FlashMode.off);
            await _controller
                .lockCaptureOrientation(DeviceOrientation.portraitUp);

            final image = await _controller.takePicture();

            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        ImageAreaSelector(image: File(image.path))));
          } catch (error) {
            showDialog(
                context: context,
                builder: (context) => const ErrorDialog(
                    message: 'An error occured while taking the picture...'));
          }
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
