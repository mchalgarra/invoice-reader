import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:image_crop/image_crop.dart';
import 'package:invoice_reader/blocs/invoice/invoice_bloc.dart';
import 'package:invoice_reader/models/invoice_model.dart';
import 'package:invoice_reader/services/reader_service.dart';
import 'package:invoice_reader/services/theme_service.dart';
import 'package:invoice_reader/screens/home.dart';
import 'package:invoice_reader/widgets/shared/error_dialog.dart';
import 'package:invoice_reader/widgets/modals/invoice_title_modal.dart';

class CropConfig {
  int originX;
  int originY;
  int width;
  int height;

  CropConfig(this.originX, this.originY, this.width, this.height);
}

class ImageAreaSelector extends StatefulWidget {
  const ImageAreaSelector(
      {Key? key, required this.image, this.keepImage, this.invoice})
      : super(key: key);

  final File image;
  final bool? keepImage;
  final InvoiceModel? invoice;

  @override
  State<ImageAreaSelector> createState() => _ImageAreaSelectorState();
}

class _ImageAreaSelectorState extends State<ImageAreaSelector> {
  final cropKey = GlobalKey<CropState>();

  ThemeService get themeService => GetIt.I<ThemeService>();
  ReaderService get readerService => GetIt.I<ReaderService>();

  double x = 0;
  double y = 0;

  double height = 0;
  double width = 0;

  bool _croppingImage = false;

  File? _cropped;

  @override
  void initState() {
    if (widget.invoice != null) {
      x = widget.invoice!.selectedArea.left;
      y = widget.invoice!.selectedArea.top;
      width = widget.invoice!.selectedArea.width;
      height = widget.invoice!.selectedArea.height;
    }

    super.initState();
  }

  @override
  void dispose() {
    if (_cropped != null) {
      _cropped!.delete();
    }

    super.dispose();
  }

  void _goToHome() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => const HomeView()));
  }

  Future<void> _saveInvoice(
      File cropped, String title, BuildContext context) async {
    String response = await readerService.readValue(cropped);

    double? price = double.tryParse(response);

    if (response.isEmpty ||
        response == 'bad-request' ||
        price == null ||
        price.isNaN) {
      showDialog(
          context: context,
          builder: (context) => const ErrorDialog(
              message: 'It was not possible to read the image...'));

      return;
    }

    if (widget.invoice != null) {
      context.read<InvoiceBloc>().add(UpdateInvoice(InvoiceModel(
          widget.invoice!.id,
          widget.invoice!.number,
          widget.invoice!.month,
          widget.invoice!.year,
          title,
          price,
          widget.invoice!.image,
          Area(left: x, top: y, width: width, height: height))));

      _goToHome();

      return;
    }

    final invoiceData = InvoiceModelCreate(
        title,
        price,
        File(widget.image.path),
        Area(left: x, top: y, width: width, height: height));

    context
        .read<InvoiceBloc>()
        .add(AddInvoice(invoiceData, widget.keepImage ?? false));

    _goToHome();
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    final previewHeight = screenSize.height - kToolbarHeight - 120;
    final previewWidth = screenSize.width;

    if (width == 0) {
      width = previewWidth;
    }

    if (height == 0) {
      height = previewHeight;
    }

    return BlocBuilder<InvoiceBloc, InvoiceState>(
      builder: (context, state) {
        return WillPopScope(
            child: Scaffold(
              appBar: AppBar(
                title: Text('Select invoice price area',
                    style: TextStyle(color: themeService.textColor)),
                centerTitle: true,
                backgroundColor: Colors.transparent,
                shadowColor: Colors.transparent,
                automaticallyImplyLeading: false,
              ),
              backgroundColor: themeService.backgroundColor,
              body: SizedBox(
                  height: previewHeight,
                  width: previewWidth,
                  child: Crop(key: cropKey, image: FileImage(widget.image))),
              floatingActionButton: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      FloatingActionButton(
                          heroTag: 'cancel',
                          onPressed: () => _goToHome(),
                          tooltip: 'Cancel',
                          backgroundColor: themeService.alertColor,
                          child: const Icon(Icons.close_rounded, size: 30)),
                      FloatingActionButton(
                          heroTag: 'action',
                          onPressed: () async {
                            try {
                              setState(() {
                                _croppingImage = true;
                              });

                              final crop = cropKey.currentState;
                              final area = crop!.area;

                              File cropped = await ImageCrop.cropImage(
                                  file: widget.image, area: area!);

                              setState(() {
                                _cropped = cropped;
                                _croppingImage = false;
                              });

                              showModalBottomSheet(
                                  context: context,
                                  backgroundColor: themeService.backgroundColor,
                                  isDismissible: false,
                                  isScrollControlled: true,
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(20.0)),
                                  builder: (context) => InvoiceTitleModal(
                                      initialTitle: widget.invoice != null
                                          ? widget.invoice!.name
                                          : null,
                                      onCancel: () {
                                        setState(() {
                                          _cropped = null;
                                        });
                                      },
                                      onSave: (String title) async {
                                        await _saveInvoice(
                                            _cropped!, title, context);
                                      }));
                            } catch (e) {
                              setState(() {
                                _croppingImage = false;
                              });

                              showDialog(
                                  context: context,
                                  builder: (context) => const ErrorDialog(
                                      message:
                                          'It was not possible to read the image...'));
                            }
                          },
                          tooltip: 'Save',
                          backgroundColor: themeService.accentColor,
                          child: _croppingImage
                              ? const SizedBox(
                                  width: 24,
                                  height: 24,
                                  child: CircularProgressIndicator(
                                      color: Colors.white, strokeWidth: 3))
                              : const Icon(Icons.crop_rounded, size: 30))
                    ],
                  )),
              floatingActionButtonLocation:
                  FloatingActionButtonLocation.centerDocked,
            ),
            onWillPop: () async {
              _goToHome();
              return false;
            });
      },
    );
  }
}
