import 'package:camera/camera.dart';
import 'package:flutter/material.dart';

class CameraContainer extends StatelessWidget {
  const CameraContainer({Key? key, required this.cameraPreview})
      : super(key: key);

  final CameraPreview cameraPreview;

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    final tileHeight = (screenSize.height) / 4;
    final tileWidth = screenSize.width / 3;

    return Stack(
      fit: StackFit.expand,
      children: [
        cameraPreview,
        GridView.count(
          childAspectRatio: tileWidth / tileHeight,
          crossAxisCount: 3,
          children: List.generate(12, (index) {
            return GridTile(
              child: Container(
                decoration: BoxDecoration(
                    border: Border(
                  bottom: BorderSide(
                    color: index < 9 ? Colors.grey : Colors.transparent,
                  ),
                  right: BorderSide(
                    color:
                        (index + 1) % 3 != 0 ? Colors.grey : Colors.transparent,
                  ),
                )),
              ),
            );
          }),
        )
      ],
    );
  }
}
