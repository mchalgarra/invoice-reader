import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:intl/intl.dart';
import 'package:invoice_reader/models/invoice_model.dart';
import 'package:invoice_reader/services/theme_service.dart';

class InvoicesTable extends StatelessWidget {
  InvoicesTable({Key? key, required this.invoices, required this.onRowTap})
      : super(key: key);

  final List<InvoiceModel> invoices;

  final Function onRowTap;

  final _currencyFormat = NumberFormat('#,##0.00', 'en_US');

  final themeService = GetIt.instance<ThemeService>();

  TextStyle _tableTextStyle() {
    return TextStyle(
      fontSize: 16,
      color: themeService.textColor,
    );
  }

  double _getMonthTotal() {
    return invoices.fold(0, (prev, curr) => curr.price + prev);
  }

  @override
  Widget build(BuildContext context) {
    return Scrollbar(
        child: ListView(
      padding: const EdgeInsets.only(bottom: 80.0),
      physics: const BouncingScrollPhysics(),
      children: [
        SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Table(
              columnWidths: const {
                0: FlexColumnWidth(2),
                1: FlexColumnWidth(8),
                2: FlexColumnWidth(3),
              },
              children: [
                TableRow(children: [
                  Column(children: [Text('#', style: _tableTextStyle())]),
                  Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [Text('Name', style: _tableTextStyle())]),
                  Column(children: [Text('\$', style: _tableTextStyle())]),
                ]),
                ...invoices.map(
                  (i) => TableRow(children: [
                    Column(children: [
                      Container(
                          padding:
                              const EdgeInsets.only(top: 16.0, bottom: 8.0),
                          child: Text(i.number.toString(),
                              style: _tableTextStyle()))
                    ]),
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          GestureDetector(
                            child: Container(
                                width: double.infinity,
                                padding: const EdgeInsets.only(
                                    top: 16.0, bottom: 8.0),
                                child: Text(i.name.toString(),
                                    style: _tableTextStyle())),
                            onTap: () {
                              onRowTap(i);
                            },
                          )
                        ]),
                    Column(children: [
                      Container(
                          padding:
                              const EdgeInsets.only(top: 16.0, bottom: 8.0),
                          child: Text(_currencyFormat.format(i.price),
                              style: _tableTextStyle()))
                    ]),
                  ]),
                ),
                TableRow(children: [
                  Column(),
                  Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                            padding: const EdgeInsets.only(top: 16.0),
                            child: Text('Total',
                                style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                    color: themeService.accentTextColor)))
                      ]),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 18.0),
                        child: (Center(
                          child: (Text(
                            _currencyFormat.format(_getMonthTotal()),
                            textAlign: TextAlign.end,
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                color: themeService.accentTextColor),
                          )),
                        )),
                      ),
                    ],
                  ),
                ])
              ],
            ))
      ],
    ));
  }
}
