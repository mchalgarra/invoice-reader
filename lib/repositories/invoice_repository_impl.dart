import 'package:get_it/get_it.dart';
import 'package:invoice_reader/models/invoice_model.dart';
import 'package:invoice_reader/services/invoice_service.dart';

class InvoiceRepositoryImpl implements InvoiceRepository {
  InvoiceService invoiceService = GetIt.I<InvoiceService>();

  @override
  Future<List<InvoiceModel>> fetchInvoices(int month, int year) async {
    await Future.delayed(const Duration(seconds: 2));
    return invoiceService.loadInvoices(month, year);
  }

  @override
  Future<List<InvoiceModel>> addInvoice(
      InvoiceModelCreate invoice, bool keepImage) {
    return invoiceService.saveInvoice(invoice, keepImage);
  }

  @override
  List<InvoiceModel> updateInvoice(InvoiceModel invoice) {
    return invoiceService.updateInvoice(invoice);
  }

  @override
  List<InvoiceModel> removeInvoice(String id, String imagePath) {
    return invoiceService.removeInvoice(id, imagePath);
  }
}

abstract class InvoiceRepository {
  Future<List<InvoiceModel>> fetchInvoices(int month, int year);
  Future<List<InvoiceModel>> addInvoice(
      InvoiceModelCreate invoice, bool keepImage);
  List<InvoiceModel> updateInvoice(InvoiceModel invoice);
  List<InvoiceModel> removeInvoice(String id, String imagePath);
}
