import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:get_it/get_it.dart';
import 'package:invoice_reader/blocs/invoice/invoice_bloc.dart';
import 'package:invoice_reader/observers/invoice_bloc_observer.dart';
import 'package:invoice_reader/repositories/invoice_repository_impl.dart';

import 'package:invoice_reader/services/camera_service.dart';
import 'package:invoice_reader/services/internal_storage_service.dart';
import 'package:invoice_reader/services/invoice_service.dart';
import 'package:invoice_reader/services/local_storage_service.dart';
import 'package:invoice_reader/services/reader_service.dart';
import 'package:invoice_reader/services/theme_service.dart';
import 'package:invoice_reader/screens/home.dart';

void setupLocator() {
  GetIt.I.registerLazySingleton(() => LocalStorageService());
  GetIt.I.registerLazySingleton(() => ThemeService());
  GetIt.I.registerLazySingleton(() => InvoiceService());
  GetIt.I.registerLazySingleton(() => CameraService());
  GetIt.I.registerLazySingleton(() => InternalStorageService());
  GetIt.I.registerLazySingleton(() => ReaderService());
}

void main() {
  WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);

  setupLocator();

  GetIt.I<LocalStorageService>()
      .loadStorage()
      .whenComplete(() => GetIt.I<ThemeService>().setLocalTheme());

  BlocOverrides.runZoned(() => runApp(const App()),
      blocObserver: InvoiceBlocObserver());
}

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

    return MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) => InvoiceBloc(InvoiceRepositoryImpl()),
          ),
        ],
        child: MaterialApp(
          title: 'Invoice Reader',
          debugShowCheckedModeBanner: false,
          theme: ThemeData(fontFamily: 'Poppins'),
          home: HomeView(onBuild: () => FlutterNativeSplash.remove()),
        ));
  }
}
