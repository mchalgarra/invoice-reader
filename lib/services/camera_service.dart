import 'package:camera/camera.dart';

class CameraService {
  List<CameraDescription> cameras = [];

  Future<void> loadCameras() async {
    cameras = await availableCameras();
  }
}
