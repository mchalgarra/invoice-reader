import 'dart:io';

import 'package:path_provider/path_provider.dart';

class InternalStorageService {
  Future<String> saveFile(File file, String name, bool keepFile) async {
    String path = await _getFilePath(name);
    file.copySync(path);

    if (!keepFile) {
      file.deleteSync();
    }

    return path;
  }

  void deleteFile(String path) {
    File file = File(path);
    file.deleteSync();
  }

  Future<String> _getFilePath(String name) async {
    Directory appDocumentsDirectory = await getApplicationDocumentsDirectory();
    return '${appDocumentsDirectory.path}/$name';
  }
}
