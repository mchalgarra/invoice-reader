import 'package:get_it/get_it.dart';
import 'package:invoice_reader/models/invoice_model.dart';
import 'package:invoice_reader/services/internal_storage_service.dart';
import 'package:invoice_reader/services/local_storage_service.dart';
import 'package:invoice_reader/utils/storage_keys.dart';
import 'package:uuid/uuid.dart';

class InvoiceService {
  LocalStorageService get localStorageService => GetIt.I<LocalStorageService>();
  InternalStorageService get internalStorageService =>
      GetIt.I<InternalStorageService>();

  final List<String> _loadedDates = [];

  List<InvoiceModel> _invoices = [];

  List<InvoiceModel> get invoices => _invoices;

  Future<List<InvoiceModel>> saveInvoice(
      InvoiceModelCreate invoiceCreate, bool keepImage) async {
    List<InvoiceModel> localInvoices = [];

    if (localStorageService.hasItem(StorageKeys.invoices)) {
      localInvoices = InvoiceModel.decode(
          localStorageService.getItem(StorageKeys.invoices) ?? []);
    }

    String invoiceId = const Uuid().v4();
    int invoiceNumber =
        localInvoices.isNotEmpty ? localInvoices.last.number + 1 : 1;

    String imagePath = await internalStorageService.saveFile(
        invoiceCreate.image, '$invoiceId.jpg', keepImage);

    InvoiceModel invoice = InvoiceModel(
        invoiceId,
        invoiceNumber,
        DateTime.now().month,
        DateTime.now().year,
        invoiceCreate.name,
        invoiceCreate.price,
        imagePath,
        invoiceCreate.selectedArea);

    await _setLocalInvoices([...localInvoices, invoice]);

    if (_loadedDates.contains('${invoice.month}-${invoice.year}')) {
      _invoices.add(invoice);
    }

    return _invoices;
  }

  List<InvoiceModel> updateInvoice(InvoiceModel invoice) {
    int index = _invoices.indexWhere((i) => i.id == invoice.id);

    _invoices[index] = invoice;
    _setLocalInvoices(invoices);

    return _invoices;
  }

  List<InvoiceModel> removeInvoice(String id, String imagePath) {
    _invoices.removeWhere((invoice) => invoice.id == id);
    _setLocalInvoices(invoices);

    internalStorageService.deleteFile(imagePath);

    return _invoices;
  }

  List<InvoiceModel> loadInvoices(int month, int year) {
    if (_loadedDates.contains('$month-$year')) {
      return _invoices;
    }

    if (!localStorageService.storageAvailable) {
      return [];
    }

    if (!localStorageService.hasItem(StorageKeys.invoices)) {
      _loadedDates.add('$month-$year');
      return [];
    }

    final data =
        InvoiceModel.decode(localStorageService.getItem(StorageKeys.invoices));

    data.retainWhere((i) => i.month == month && i.year == year);

    _loadedDates.add('$month-$year');

    _invoices = [..._invoices, ...data];
    _invoices.sort((i1, i2) => i1.number - i2.number);

    return _invoices;
  }

  Future<void> _setLocalInvoices(List<InvoiceModel> invoices) async {
    await localStorageService.setItem(
        StorageKeys.invoices, InvoiceModel.encode(invoices));
  }
}
