import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:invoice_reader/services/local_storage_service.dart';
import 'package:invoice_reader/utils/storage_keys.dart';

class ThemeService {
  LocalStorageService get localStorageService => GetIt.I<LocalStorageService>();

  bool darkTheme = false;

  Color get accentColor => const Color(0xFF5D5FEF);
  Color get accentTextColor => const Color(0xFF87A3FF);
  Color get alertColor => const Color(0xFFFF5555);
  Color get textColor => darkTheme ? Colors.white : const Color(0xFF1D1D1D);
  Color get backgroundColor =>
      darkTheme ? const Color(0xFF1D1D1D) : const Color(0xFFF5F5F5);

  void toggleTheme() {
    darkTheme = !darkTheme;
    localStorageService.setItem(StorageKeys.theme, darkTheme);
  }

  void setLocalTheme() {
    darkTheme = localStorageService.getItem(StorageKeys.theme) ?? false;
  }
}
