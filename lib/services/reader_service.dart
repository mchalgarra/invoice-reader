import 'dart:io';

import 'package:http/http.dart' as http;

class ReaderService {
  final String _apiUrl = 'https://invoice-reader-api.herokuapp.com/api';

  Uri get apiReaderUri => Uri.parse('$_apiUrl/reader');

  Future<String> readValue(File image) async {
    try {
      http.MultipartRequest request =
          http.MultipartRequest("POST", apiReaderUri);

      request.files.add(await http.MultipartFile.fromPath('file', image.path));
      request.headers['Content-Type'] = 'multipart/form-data';

      http.StreamedResponse response = await request.send();
      String responseBody = await response.stream.bytesToString();

      if (response.statusCode == 400) {
        return 'bad-request';
      }

      String price = responseBody.replaceAll(RegExp('[^.,0-9]'), '');
      String parsedPrice = price
          .replaceFirst(RegExp('[,|.](?=([\\d]{1,2})\$)'), '-')
          .replaceAll(RegExp('[,|.]'), '')
          .replaceAll('-', '.');

      return parsedPrice;
    } on Exception {
      throw Error();
    }
  }
}
