import 'dart:convert';

import 'dart:io';

class Area {
  double left;
  double top;
  double width;
  double height;

  Area({this.left = 0.0, this.top = 0.0, this.width = 0.0, this.height = 0.0});
}

class InvoiceModelCreate {
  String name;
  double price;

  File image;
  Area selectedArea;

  InvoiceModelCreate(this.name, this.price, this.image, this.selectedArea);
}

class InvoiceModel {
  String id;
  int number;
  int month;
  int year;
  String name;
  double price;

  String image;
  Area selectedArea;

  InvoiceModel(this.id, this.number, this.month, this.year, this.name,
      this.price, this.image, this.selectedArea);

  factory InvoiceModel.fromJson(Map<String, dynamic> jsonData) {
    return InvoiceModel(
        jsonData['id'],
        jsonData['number'],
        jsonData['month'],
        jsonData['year'],
        jsonData['name'],
        jsonData['price'],
        jsonData['image'],
        Area(
            left: jsonData['left'],
            top: jsonData['top'],
            width: jsonData['width'],
            height: jsonData['height']));
  }

  static Map<String, dynamic> toMap(InvoiceModel invoice) => {
        'id': invoice.id,
        'number': invoice.number,
        'month': invoice.month,
        'year': invoice.year,
        'name': invoice.name,
        'price': invoice.price,
        'image': invoice.image,
        'left': invoice.selectedArea.left,
        'top': invoice.selectedArea.top,
        'width': invoice.selectedArea.width,
        'height': invoice.selectedArea.height
      };

  static String encode(List<InvoiceModel> invoices) => json.encode(
        invoices
            .map<Map<String, dynamic>>((invoice) => InvoiceModel.toMap(invoice))
            .toList(),
      );

  static List<InvoiceModel> decode(String invoices) =>
      (json.decode(invoices) as List<dynamic>)
          .map<InvoiceModel>((item) => InvoiceModel.fromJson(item))
          .toList();
}
